#include "PID.hpp"
#include<Pixy2.h>
Pixy2 pixy;
constexpr uint8_t speedRight = 5,
                  speedLeft = 3,
                  dirRight = 7,
                  dirLeft = 6,
                  brakeRight = 8,
                  brakeLeft = 9;

bool forwardLeft = true, 
     forwardRight = true;

int16_t speedValue = 0;
uint32_t currentTime, lastTime; 

constexpr uint16_t dt = 25;

//#define TUNING //Decommentare per abilitare il tuning bluetooth
#define PID1 0 
#define PID2 0
#ifdef TUNING
fixed Kp1=0,Ki1=0,Kd1=0;
fixed Kp2=0,Ki2=0,Kd2=0;
PID_IC pid1(Kp1, Ki1, Kd1, 2 * M_PI * 60, dt);
PID_IC pid2(30, 10, 30, 2 * M_PI * 60, dt);
#else
PID_IC pid1(48, 0.5, 40, 2 * M_PI * 60, dt); 
PID_IC pid2(30, 10, 30, 2 * M_PI * 60, dt);
#endif

int getDistance(int x) {
  return 68.401169 * exp(-0.013394 * x); //la formula è ottenuta tramite una regressione non lineare
}

void setup() {
  uint8_t pins[] = {speedLeft, speedRight, brakeLeft, brakeRight, dirLeft, dirRight};
  for (uint8_t i = 0; i < sizeof(pins) / sizeof(pins[0]); i++) {
    pinMode(pins[i], OUTPUT);
  }
#ifdef TUNING
  Serial.begin(9600);
#endif
  pixy.init();
  pixy.setLamp(255, 0);

  pid1.setPoint = 20;
  pid1.uLimit = 255;
  pid1.lLimit = -255;

  pid2.setPoint = 158;
  pid2.uLimit = 255;
  pid2.lLimit = -255;
  currentTime = lastTime = millis();
}
void loop() {
  uint8_t speedLeftValue = 0;
  uint8_t speedRightValue = 0;
  currentTime = millis();
  if (currentTime - lastTime > dt && pixy.ccc.getBlocks()) {
    int D = getDistance(pixy.ccc.blocks[0].m_height);
#ifdef PID1
    speedValue = (int)pid1.compute(D);
    if (speedValue > 0) {
      forwardLeft = forwardRight = true;
    }
    else {
      forwardLeft = forwardRight = false;
    }
    speedLeftValue = abs(speedValue);
    speedRightValue = speedLeftValue;
#endif
#ifdef PID2
    int s = pid2.compute(pixy.ccc.blocks[0].m_x);
    if (s > 0) {
      if (forwardRight) {
        speedLeftValue -= abs(s);
        if (speedLeftValue > 0) speedLeftValue = 0;
      }
      else {
        speedRightValue -= abs(s);
        if (speedRightValue > 0) speedRightValue = 0;
      }
    }
    else {
      if (forwardRight) {
        speedRightValue -= abs(s);
        if (speedRightValue > 0) speedRightValue = 0;
      }
      else {
        speedLeftValue -= abs(s);
        if (speedLeftValue > 0) speedLeftValue = 0;
      }
    }
#endif
  }
  digitalWrite(dirRight, forwardRight);
  digitalWrite(dirLeft, forwardLeft);
  analogWrite(speedLeft, speedLeftValue);
  analogWrite(speedRight, speedRightValue);
#ifdef TUNING
  if (Serial.available()) {
    char c = Serial.read();
    switch (c) {
      case 'P':
        Kp1 = Serial.parseInt() / 10.0;
        pid1.updatePIDgains(Kp1, Ki1, Kd1);
        break;
      case 'p':
        Kp2 = Serial.parseInt() / 10.0;
        pid2.updatePIDgains(Kp2, Ki2, Kd2);
        break;
      case 'I':
        Ki1 = Serial.parseInt() / 10.0;
        pid1.updatePIDgains(Kp1, Ki1, Kd1);
        break;
      case 'i':
        Ki2 = Serial.parseInt() / 10.0;
        pid2.updatePIDgains(Kp2, Ki2, Kd2);
        break;
      case 'D':
        Kd1 = Serial.parseInt() / 10.0;
        pid1.updatePIDgains(Kp1, Ki1, Kd1);
        break;
      case 'd':
        Kd2 = Serial.parseInt() / 10.0;
        pid2.updatePIDgains(Kp2, Ki2, Kd2);
        break;
      case 'N':
        Serial.println("*D PID1 Kp=" + String((float)Kp1) + " , Ki=" + String((float)Ki1) + " , Kd=" + String((float)Kd1) + "\n*");
        Serial.println("*D PID2 Kp=" + String((float)Kp2) + " , Ki=" + String((float) Ki2) + " , Kd=" + String((float)Kd2) + "\n*");
    }
    Serial.read();
  }
#endif
}
