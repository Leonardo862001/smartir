#pragma once
#include "fixed_point.hpp"
class PID_BC {
  public:
    PID_BC(fixed Kp, fixed Ki, fixed Kd, fixed Kn, fixed N, uint16_t dt) : _Kp(Kp), _Ki(Ki * dt * 5e-4), _Kd((2 * Kd) / (2 + dt / (1e3 * N))), _Kf((2 - dt / (N * 1e3)) / (2 - dt / (N * 1e3))), _Kn(Kn), _N(N), _dt(dt) {}
    void updatePIDgains(fixed Kp, fixed Ki, fixed Kd) {
      float T_sec = _dt / 1e3;
      _Kp = Kp;
      _Ki = Ki * T_sec * 0.5;
      _Kd = (2 * Kd) / (2 + T_sec / _N);
      _Kf = (2 - T_sec / _N) / (2 + T_sec / _N);
    }
    void setLimit(fixed upper, fixed lower){
	    uLimit=upper;
	    lLimit=lower;
    }
    fixed compute(fixed input) {
      fixed e = setPoint - input;
      fixed output = _Kp * e;
      fixed d = (e - lastError) * _Kd + _Kf * lastDerivative;
      output += d;
      errorAccumulator += (_Ki * (e + lastError) + _Kn * (lastSaturatedOutput - lastOutput));
      output += errorAccumulator;
      lastError = e;
      lastDerivative = d;
      lastOutput = output;
      if (output > uLimit) output = uLimit;
      if (output < lLimit) output = lLimit;
      lastSaturatedOutput = output;
      return output;
    }
    fixed _Kp, _Ki, _Kd, _Kn, _Kf;
    fixed _N;
    uint16_t _dt;
    uint16_t setPoint;
    fixed uLimit, lLimit;
    fixed errorAccumulator = 0, lastError = 0, lastDerivative = 0, lastOutput = 0, lastSaturatedOutput = 0;
};

class PID_IC {
  public:
    PID_IC(fixed Kp, fixed Ki, fixed Kd, fixed N, uint16_t dt) : _Kp(Kp), _Ki(Ki * dt * 5e-4), _Kd((2 * Kd) / (2 + dt / (1e3 * N))), _Kf((2 - dt / (N * 1e3)) / (2 - dt / (N * 1e3))), _N(N), _dt(dt) {}
    void updatePIDgains(fixed Kp, fixed Ki, fixed Kd) {
      float T_sec = _dt / 1e3;
      _Kp = Kp;
      _Ki = Ki * T_sec * 0.5;
      _Kd = (2 * Kd) / (2 + T_sec / _N);
      _Kf = (2 - T_sec / _N) / (2 + T_sec / _N);
    }
    void setLimit(fixed upper, fixed lower){
	    uLimit=upper;
	    lLimit=lower;
    }
    fixed compute(fixed input) {
      fixed e = setPoint - input;

      fixed output = _Kp * e;

      fixed d = (e - lastError) * _Kd + _Kf * lastDerivative;
      output += d;

      if (!((lastOutput != lastSaturatedOutput) && (e * lastOutput > static_cast<fixed>(0)))) {
        errorAccumulator += e + lastError;
        output += _Ki * errorAccumulator;
      }

      lastError = e;
      lastDerivative = d;
      lastOutput = output;

      if (output > uLimit) output = uLimit;
      if (output < lLimit) output = lLimit;

      lastSaturatedOutput = output;

      return output;
    }
    fixed _Kp, _Ki, _Kd, _Kf;
    fixed _N;
    uint16_t _dt;
    uint16_t setPoint;
    fixed uLimit, lLimit;
    fixed errorAccumulator = 0, lastError = 0, lastDerivative = 0, lastOutput = 0, lastSaturatedOutput = 0;
};

class PID {
  public:
    PID(fixed Kp, fixed Ki, fixed Kd, fixed N, uint16_t dt) : _Kp(Kp), _Ki(Ki * dt * 5e-4), _Kd((2 * Kd) / (2 + dt / (1e3 * N))), _Kf((2 - dt / (N * 1e3)) / (2 - dt / (N * 1e3))), _N(N), _dt(dt) {}
    void updatePIDgainsgains(fixed Kp, fixed Ki, fixed Kd) {
      float T_sec = _dt / 1e3;
      _Kp = Kp;
      _Ki = Ki * T_sec * 0.5;
      _Kd = (2 * Kd) / (2 + T_sec / _N);
      _Kf = (2 - T_sec / _N) / (2 + T_sec / _N);
    }
    void setLimit(fixed upper, fixed lower){
	    uLimit=upper;
	    lLimit=lower;
    }
    fixed compute(fixed input) {
      fixed e = setPoint - input;

      fixed output = _Kp * e;

      fixed d = (e - lastError) * _Kd + _Kf * lastDerivative;
      output += d;

      errorAccumulator += e + lastError;
      output += _Ki * errorAccumulator;

      lastError = e;
      lastDerivative = d;

      if (output > uLimit) output = uLimit;
      if (output < lLimit) output = lLimit;

      return output;
    }
    fixed _Kp, _Ki, _Kd, _Kf;
    fixed _N;
    uint16_t _dt;
    uint16_t setPoint;
    fixed uLimit, lLimit;
    fixed errorAccumulator = 0, lastError = 0, lastDerivative = 0;
};
