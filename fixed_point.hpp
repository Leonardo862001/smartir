#include<stdint.h>
#pragma once
#define SCALE 8
class fixed{
	private:
		int32_t x;
	public:
		fixed() : x(0) {}

		fixed(int16_t val) : x(val<<SCALE) {}
		fixed(int32_t val) :x(val<<SCALE) {}
		fixed(long long int val) : x((long long int)(val<<SCALE)) {}
		fixed(uint16_t val) : x(val<<SCALE) {}
		fixed(uint32_t val) : x(val<<SCALE) {}
		fixed(float val) : x((int32_t)(val*(float)(1<<SCALE))) {}
		fixed(double val) : x((int32_t)(val*(double)(1<<SCALE))) {}

		operator int16_t() const { int16_t t; t=x&(1<<(SCALE-1))? (x>>SCALE)+1 : (x>>SCALE); return t; }
		operator uint16_t() const { uint16_t t; t=x&(1<<(SCALE-1))? (x>>SCALE)+1 : (x>>SCALE); return t; }
		operator int32_t() const { int32_t t; t=x&(1<<(SCALE-1))? (x>>SCALE)+1 : (x>>SCALE); return t; }
		operator uint32_t() const { uint32_t t; t=x&(1<<(SCALE-1))? (x>>SCALE)+1 : (x>>SCALE); return t; }
		operator long long int() const { long long int t; t=x&(1<<(SCALE-1))? (x>>SCALE)+1 : (x>>SCALE); return t; }
		operator float() const { return (float)this->x/(float)(1<<SCALE); }
		operator double() const { return (double)this->x/(double)(1<<SCALE); }

		fixed& operator=(const int16_t val) { x=(int32_t)val<<SCALE; return *this; }
		fixed& operator=(const uint16_t val) { x=val<<SCALE; return *this; }
		fixed& operator=(const int32_t val) { x=val<<SCALE; return *this; }
		fixed& operator=(const uint32_t val) { x=val<<SCALE; return *this; }
		fixed& operator=(const float val) { x=(int32_t)(val*(float)(1<<SCALE)); return *this; }
   		fixed& operator=(const double val) { x=(int32_t)(val*(double)(1<<SCALE)); return *this; }

		fixed operator+(const fixed& rhs) { fixed t; t.x=x+rhs.x; return t; }
		template<typename T>
		fixed operator+(const T& rhs) { return *this+static_cast<fixed>(rhs); }

    	fixed operator-() {this->x=-this->x; return *this; }
		fixed operator-(const fixed& rhs) { fixed t; t.x=x-rhs.x; return t; }
		template<typename T>
		fixed operator-(const T& rhs) { return *this-static_cast<fixed>(rhs); }

		fixed operator*(const fixed& rhs) { fixed t; t.x = ((int64_t)this->x*rhs.x)>>SCALE; return t; }
		template<typename T>
		fixed operator*(const T& rhs) { return *this*static_cast<fixed>(rhs); }

		fixed operator/(const fixed& rhs) { fixed t; t.x=((int64_t)(this->x)<<SCALE)/rhs.x; return t; }
		template<typename T>
		fixed operator/(const T& rhs) { return *this/static_cast<fixed>(rhs); }

		fixed operator+=(const fixed& rhs) { *this=*this+rhs; return *this; } 
		template<typename T>
		fixed operator+=(const T& rhs) { *this=*this+static_cast<fixed>(rhs); return *this; } 

		fixed operator-=(const fixed& rhs) { *this=*this-rhs; return *this; } 
		template<typename T>
		fixed operator-=(const T& rhs) { *this=*this-static_cast<fixed>(rhs); return *this; } 

		fixed operator*=(const fixed& rhs) { *this=*this*rhs; return *this; } 
		template<typename T>
		fixed operator*=(const T& rhs) { *this=*this*static_cast<fixed>(rhs); return *this; } 

		fixed operator/=(const fixed& rhs) { *this=*this/rhs; return *this; } 
		template<typename T>
		fixed operator/=(const T& rhs) { *this=*this/static_cast<fixed>(rhs); return *this; } 

		inline fixed operator<<(const uint16_t b) { fixed t; t.x=x<<b; return t; }
		inline fixed operator<<(const int16_t b) { fixed t; t.x=x<<b; return t; }
		inline fixed operator>>(const uint16_t b) { fixed t; t.x=x>>b; return t; }
		inline fixed operator>>(const int16_t b) { fixed t; t.x=x>>b; return t; }

		inline bool operator==(const fixed& rhs) { return this->x==rhs.x; }
		inline bool operator!=(const fixed& rhs) { return this->x!=rhs.x; }
		inline bool operator>(const fixed& rhs) { return this->x>rhs.x; }
		inline bool operator<(const fixed& rhs) { return this->x<rhs.x; }
#ifdef _GLIBCXX_IOSTREAM
		friend std::ostream& operator<<(std::ostream& out, const fixed& a){
			float t = (float)a.x/(float)(1<<SCALE);
			return out << t;
		}
		friend std::istream& operator>>(std::istream& in,fixed& a){
			float temp;
			in >> temp;
			a=temp;
			return in;
		}
#endif
};

template<typename T>
fixed operator+(const T& lhs, const fixed& rhs) { return static_cast<fixed>(lhs)+rhs; }
template<typename T>
fixed operator-(const T& lhs, const fixed& rhs) { return static_cast<fixed>(lhs)-rhs; }
template<typename T>
fixed operator*(const T& lhs, const fixed& rhs) { return static_cast<fixed>(lhs)*rhs; }
template<typename T>
fixed operator/(const T& lhs, const fixed& rhs) { return static_cast<fixed>(lhs)/rhs; }
template<typename T>
fixed operator+=(T& lhs, const fixed& rhs) { return lhs+=static_cast<T>(rhs); }
template<typename T>
fixed operator-=(T& lhs, const fixed& rhs) { return lhs-=static_cast<T>(rhs); }
template<typename T>
fixed operator*=(T& lhs, const fixed& rhs) { return lhs*=static_cast<T>(rhs); }
template<typename T>
fixed operator/=(T& lhs, const fixed& rhs) { return lhs/=static_cast<T>(rhs); }
template<typename T>
inline bool operator==(const T& lhs, const fixed& rhs) { return static_cast<fixed>(lhs)==rhs; }
template<typename T>
inline bool operator>(const T& lhs, const fixed& rhs) { return static_cast<fixed>(lhs)>rhs; }
