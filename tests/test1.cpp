#include "../fixed_point.hpp"
#include "gtest/gtest.h"
#include<stdint.h>
#include <stdlib.h>
#include <time.h>
TEST(fixed_test, addition) {
	fixed a = 5.391;
	fixed b = 2.875;
	EXPECT_EQ(a+b,b+a);
	EXPECT_EQ((int)(a+b),8); 
	EXPECT_NEAR((float)(a+b), 8.25f, 0.125);
}
TEST(fixed_test, subtraction) {
	fixed a = 5.391;
	fixed b = 2.875;
	EXPECT_EQ(a-b,-(b-a));
	EXPECT_EQ((int)(a-b),3); 
	EXPECT_NEAR((float)(a-b), 2.5f, 0.125);
}
TEST(fixed_test, product) {
	fixed a = 5.391;
	fixed b = 2.875;
	EXPECT_EQ(a*b,b*a);
	EXPECT_EQ((int)(a*b),15); 
	EXPECT_NEAR((float)(a*b), 15.5f, 0.125);
}
TEST(fixed_test, division) {
	fixed a = 5.391;
	fixed b = 2.875;
	EXPECT_NEAR((float)(a/b),(float)(1/(b/a)), 0.25);
	EXPECT_EQ((int)(a/b),2); 
	EXPECT_NEAR((float)(a/b), 1.88f, 0.125);
}
TEST(fixed_test, addition_float){
	fixed a = 5.391;
	float b = 2.875;
	EXPECT_EQ(a+b,b+a);
	EXPECT_EQ((int)(a+b),8); 
	EXPECT_NEAR((float)(a+b), 8.25f, 0.125);
}
TEST(fixed_test, subtraction_float){
	fixed a = 5.391;
	float b = 2.875;
	EXPECT_EQ(a-b,-(b-a));
	EXPECT_EQ((int)(a-b),3); 
	EXPECT_NEAR((float)(a-b), 2.5f, 0.125);
}
TEST(fixed_test, product_float){
	fixed a = 5.391;
	float b = 2.875;
	EXPECT_EQ(a*b,b*a);
	EXPECT_EQ((int)(a*b),15); 
	EXPECT_NEAR((float)(a*b), 15.5f, 0.125);
}
TEST(fixed_test, division_float){
	fixed a = 5.391;
	float b = 2.875;
	EXPECT_NEAR((float)(a/b),(float)(1/(b/a)), 0.25);
	EXPECT_EQ((int)(a/b),2); 
	EXPECT_NEAR((float)(a/b), 1.88f, 0.125);
}
float random_float(){
	return 255 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(255)));
}
TEST(random_test, addition){
	for(int i = 0; i < 100; i++){
	float a=random_float();
	float b=random_float();
	fixed c = a;
	fixed d = b;
	ASSERT_NEAR(a, (float)(c), 0.25);
	EXPECT_NEAR(a+b,(float)(c+d), 0.25);
	}
}
TEST(random_test, subtraction){
	for(int i = 0; i < 100; i++){
	float a=random_float();
	float b=random_float();
	fixed c = a;
	fixed d = b;
	ASSERT_NEAR(a, (float)(c), 0.25);
	EXPECT_NEAR(a-b,(float)(c-d), 0.25);
	}
}
TEST(random_test, product){
	for(int i = 0; i < 100; i++){
	float a=random_float();
	float b=random_float();
	fixed c = a;
	fixed d = b;
	ASSERT_NEAR(a, (float)(c), 0.25);
	EXPECT_NEAR(a*b,(float)(c*d), (255*255)/5000);
	}
}
TEST(random_test, division){
	for(int i = 0; i < 100; i++){
	float a=random_float();
	float b=random_float();
	fixed c = a;
	fixed d = b;
	ASSERT_NEAR(a, (float)(c), 0.25);
	EXPECT_NEAR(a/b,(float)(c/d), 0.25);
	}
}
int main(int argc, char **argv) {
  srand(time(NULL));
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
